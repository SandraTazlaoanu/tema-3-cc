#include <stdlib.h>

#include "ZodiacResponse.h"

ZodiacResponse::ZodiacResponse() : Response("Zodiac")
{

}

bool ZodiacResponse::Validare(int year, int month, int day)
{
	if (year < 2019 && year > 0)
	{
		if (month >= 1 && month <= 12)
		{
			if (day >= 1 && day <= 31)
			{
				if ((year % 4 == 0) && month == 2 && day <= 29) 
					return true;
				else 
					if ((year % 4 == 0) && month == 2 && day > 29) 
						return false;
				if ((year % 4 != 0) && month == 2 && day <= 28) 
					return true;
				else if ((year % 4 != 0) && month == 2 && day > 28) 
					return false;
				if ((month == 4 || month == 6 || month == 9 || month == 11) && day <= 30)
					return true;
				if ((month == 4 || month == 6 || month == 9 || month == 11) && day > 30) 
					return false;
				return true;
			}
		}
		return false;
	}
	return false;
}

std::string ZodiacResponse::Zodiac(int year, int month, int day)
{
	if ((month == 3 && day >= 21) || (month == 4 && day <= 20))
		return "Berbec -> 21 Martie-20 Aprilie";
	if ((month == 4 && day >= 21) || (month == 5 && day <= 20))
		return "Taur -> 21 Aprilie-20 Mai";
	if ((month == 5 && day >= 21) || (month == 6 && day <= 21))
		return "Gemeni -> 21 Mai-21 Iunie";
	if ((month == 6 && day >= 22) || (month == 7 && day <= 22))
		return "Rac -> 22 Iunie-22 Iulie";
	if ((month == 7 && day >= 23) || (month == 8 && day <= 22))
		return "Leu -> 23 Iulie-22 August";
	if ((month == 8 && day >= 23) || (month == 9 && day <= 22))
		return "Fecioara -> 23 August-22 Septembrie";
	if ((month == 9 && day >= 23) || (month == 10 && day <= 22))
		return "Balanta -> 23 Septembrie-22 Octombrie";
	if ((month == 10 && day >= 23) || (month == 11 && day <= 21))
		return "Scorpion -> 23 Octombrie-21 Noiembrie";
	if ((month == 11 && day >= 23) || (month == 12 && day <= 20))
		return "Sagetator -> 22 Noiembrie-20 Decembrie";
	if ((month == 12 && day >= 21) || (month == 1 && day <= 19))
		return "Capricorn -> 21 Decembrie-19 Ianuarie";
	if ((month == 1 && day >= 20) || (month == 2 && day <= 18))
		return "Varsator -> 20 Ianuarie-18 Februarie";
	return "Pesti -> 19 Februarie-20 Martie";
}



std::string ZodiacResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	auto year = atoi(packet.get<std::string>("Year").c_str());
	auto month = atoi(packet.get<std::string>("Month").c_str());
	auto day = atoi(packet.get<std::string>("Day").c_str());

	this->content.push_back(boost::property_tree::ptree::value_type("File", "zodiac.txt"));
	if (Validare(year, month, day) == false)
		this->content.push_back(boost::property_tree::ptree::value_type("Result", "Data nu e valida!"));
	else
	{
		this->content.push_back(boost::property_tree::ptree::value_type("Result", Zodiac(year, month, day)));
	}

	return this->getContentAsString();
}
