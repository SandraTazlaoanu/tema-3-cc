#include "ResponsesManager.h"

#include "HandshakeResponse.h"
#include "SumResponse.h"
#include "WordCountResponse.h"
#include "ZodiacResponse.h"

ResponsesManager::ResponsesManager()
{
	this->Responses.emplace("Handshake", std::make_shared<HandshakeResponse>());
	this->Responses.emplace("Sum", std::make_shared<SumResponse>());
	this->Responses.emplace("WordCounter", std::make_shared<WordCountResponse>());
	this->Responses.emplace("Zodiac", std::make_shared<ZodiacResponse>());
}

std::map<std::string, std::shared_ptr<Framework::Response>> ResponsesManager::getMap() const
{
	return this->Responses;
}
