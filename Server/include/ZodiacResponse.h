#pragma once
#include <string>
#include "Response.h"

class ZodiacResponse : public Framework::Response
{
public:
	ZodiacResponse();
	bool Validare(int year, int month, int day);
	std::string Zodiac(int year, int month, int day);

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};