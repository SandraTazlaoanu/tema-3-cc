#include "ZodiacRequest.h"

ZodiacRequest::ZodiacRequest() : Request("Zodiac")
{
	this->content.push_back(boost::property_tree::ptree::value_type("Year", "1998"));
	this->content.push_back(boost::property_tree::ptree::value_type("Month", "7"));
	this->content.push_back(boost::property_tree::ptree::value_type("Day", "9"));
}
